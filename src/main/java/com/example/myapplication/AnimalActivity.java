package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);
        TextView nom1 = (TextView)findViewById(R.id.animal1);
        TextView esp=findViewById(R.id.esp);
        TextView periode=findViewById(R.id.peri);
        TextView poidnais=findViewById(R.id.pnais);
        TextView poidadult=findViewById(R.id.tpadult);
        final EditText statut=findViewById(R.id.vulnerabilité);
        ImageView avatar=findViewById(R.id.avatar);
        Button sauv=findViewById(R.id.sauv);


        Intent act=getIntent();
        final String anim=act.getStringExtra("key");
        nom1.setText(anim);

        AnimalList list=new AnimalList();
        String [] ls=list.getNameArray();
        for(int i=0;i<7;i++){
            if(ls[i].equals(anim)){
                final Animal a=list.getAnimal(ls[i]);
                esp.setText(a.getStrHightestLifespan());
                periode.setText(a.getStrGestationPeriod());
                poidnais.setText(a.getStrBirthWeight());
                poidadult.setText(a.getStrAdultWeight());
                statut.setText(a.getConservationStatus());
                String photo=a.getImgFile();
                avatar.setImageResource(getResources().getIdentifier(photo, "drawable",  getPackageName()));

                sauv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        a.setConservationStatus(statut.getText().toString());

                        Toast.makeText(AnimalActivity.this,"sauvgarder ",Toast.LENGTH_LONG).show();
                    }
                });
            }

        }



    }
}
