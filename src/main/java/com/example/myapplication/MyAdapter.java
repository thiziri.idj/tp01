package com.example.myapplication;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    String nom [];
    int image [];
    Context cont;

    public MyAdapter(Context ct, String animal [], int icone []){
        cont=ct;
        nom=animal;
        image=icone;

    }
    
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(cont);
        View view= inflater.inflate(R.layout.my_row,parent,false);

        return new MyViewHolder(view);
    }
//applique l'image et le texte qui faut sur la sructure pour le metre dans chaque iteme du recyclerview
    //prend en charge la selection de l'iteme et le lancement du la seconde actuvity
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.nom1.setText(nom[position]);
        holder.img.setImageResource(image[position]);
        holder.cl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(cont, AnimalActivity.class);
                intent.putExtra("key",nom[position]);
                cont.startActivity(intent);

            }
        });

    }
//recupere la structure de de chaque iteme image plus textview qui se trouve dans my_row
    @Override
    public int getItemCount() {
        return nom.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nom1;
        ImageView img;
        ConstraintLayout cl;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            nom1=itemView.findViewById(R.id.animal);
            img=itemView.findViewById(R.id.my_imageView);
            cl=itemView.findViewById(R.id.mainlayout);
        }
    }
}

