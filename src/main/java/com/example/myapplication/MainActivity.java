package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        final RecyclerView rv = (RecyclerView) findViewById(R.id.rcanimal);
        //String [] values=new String[]{"Renard roux","Koala","Montbéliarde","Panda géant","Ours brun","Chameau","Lion"} ;
        String animal[];
        int [] icone=new int[]{R.drawable.fox,R.drawable.koala,R.drawable.cow,R.drawable.panda,R.drawable.bear,R.drawable.camel,R.drawable.lion};
        animal=getResources().getStringArray(R.array.animal);
        MyAdapter myAdapter=new MyAdapter(this,animal,icone);
        rv.setAdapter(myAdapter);
        rv.setLayoutManager(new LinearLayoutManager(this));

    }
}
